# Certified Red Team Lead (CRTL)

|Date taken|July - August 2024|
|Overall Rating|3/5|

While I found both the course material and the exam good on their own, they do not match well in my opinion.
This leads me to only give a "maybe" recommendation for the course, although I liked both the course and the exam.
But to be fair, I will rate both the course material and the exam on their own.

## Course

L|Homepage|https://training.zeropointsecurity.co.uk/courses/red-team-ops-ii|
|Rating|4/5|

The course content is interesting, but much shorter than the CRTO.
There also seems to no longer be any lab time included.
However, setting up a Windows VM with Visual Studio was not that hard and doing the programming on your own computer is helpful, since it leaves you with a working setup.

## Exam

L|Homepage|https://training.zeropointsecurity.co.uk/pages/red-team-ops-ii-exam|
|Rating|5/5|

Aside from the mismatch with the course material, the exam was very fun and I learned about one new tool.
Personally I liked it much more than the CRTO exam, since it required some thinking and not just copying commands from the course material.
The exam also seems to have changed from the initial version, since all reviews of it mention needing 4 of 4 flags to pass and having 5 days to find them.
When I took the exam it required 5 of 6 flags to be found in 8 days.

During the exam I pretty much only used the ETW patches with `execute-assambly` and `powerpick` from the new course material.
Otherwise I pretty much used the same C2 configuration (just added default sleep kit) as for the CRTO and the same attack techniques (I looked at the CRTO course material a lot) and did not need to use any of the fancy new techniques shown (like argument spoofing, PPID spoofing, API unhooking, custom code for process injection, working around WDAC rule, removing indicators from executables, etc).
Some of that may be the fault of the being able to prevent Windows Defender from killing my beacons using a simple (cheesy) strategy.
But a lot of the course material not mattering is because of the design and the configuration of the exam environment.
By modifying the exam these aspects could have been tested too, for example:

- Adding WDAC with certificates required (at least in `C:\Windows\` / `ADMIN$`) for some of the machines.
- Not making it assume breach or just giving us a laptop not connected to the rest of the network, so that you need to abuse ASR holes and Macros with a phishing mail.
- Allowing and requiring us to bypass the Protected Process status of `lsass.exe`.

Also it is a bit annoying, that I did not receive any lab time and I could not find some private scripts (`wd-extract.py` from `Reversing ASR exclusions`) in the exam VM (so I would need to pay extra for lab time to get the scripts).

There is the issue with the VM without Internet again, but I only came up against it twice.
Once be because one of the tools was not well documented (so I had to do some research) and another time where I was able to bypass the issue by copying an external tool (that I am much more familiar with) in.
Overall it is solvable with the given tools, especially given the 8 days that you have to solve the exam.

### Exam progress

Since I did the CRTO a couple months before that, the setup worked relatively quick and I got the first two flags relatively quickly.
Getting the third flag took a while and copying an external tool into the lab environment, but in retrospect it was pretty obvious where to look.
Getting the last flag almost took a day, since it seemed obvious what to do, but it always failed.
So I tried in many different ways to get it and pretty much all of them failed, until my research finally paid of and I could fix my error.

As you can see in my timeline I got all my flags in the morning or around noon.
This just shows how useful it can be to sleep on a problem and try again with fresh eyes, which the exam luckily allows due to the gracious time limit.

![Exam page](../assets/img/crtl-exam-graph.png)

