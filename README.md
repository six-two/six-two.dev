# six-two.dev

My personal webpage

## Common tasks

Show site (live reloading):
```bash
mkdocs serve
```

Build page (output in `site/` folder):
```bash
mkdocs build
```

Update pinned requirements (requires `pip-tools` from PyPI):
```bash
pip-compile -U
```

Wrapper script that uses a virtual python environment and serves the site:
```bash
./serve.sh
```

Update the dependencies in the virtual python environment and serve the site:
```bash
./serve.sh update
``` 
