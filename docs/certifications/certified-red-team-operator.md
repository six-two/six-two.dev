# Certified Red Team Operator (CRTO)

|Date taken|March - June 2024|
|Overall Rating|5/5|

Overall it is a great course available for a pretty good price.
I can recommend it for anyone with an interest in red teaming or Active Directory exploitation in general.
It was so good that I bought a couple other courses from Zero-Point Security like the the C# one and the sequel `Red Team Ops II`.

## Course & Lab

L|Homepage|https://training.zeropointsecurity.co.uk/courses/red-team-ops|
|Rating|5/5|

I find the course material very good.
In fact it is so good that I have dumped it all and added it to my private notes collection.
Especially nice is the fact that you get lifetime access to the course.

Some of the topics (like Kerberos) I had almost no prior experience with, but how to exploit and use them are explained well and succinct.
I also liked the lab and spent a lot of time (probably around 80 hours) in it.
Although a lot of that was spent playing around with [custom Cobalt Strike modules that offer some quality of life features](https://gitlab.com/six-two/bin/-/tree/main/pentest-tools/cobalt-strike?ref_type=heads) and trying to make a tool that copies data in and out of guacamole by chunking it via the clipboard (that failed, since the clipboard is very unreliable).

## Exam

L|Homepage|https://training.zeropointsecurity.co.uk/pages/red-team-ops-exam|
|Rating|4/5|

Overall the exam tests many of the techniques shown in the course and was pretty fun.
I especially like how the exam gives you plenty of time and the option to pause it, so that you can take a break and for example take a short walk when you are stuck somewhere instead of stressing you.
Also very nice is that there are some "extra mile" challenges (flag 7 and 8) that are harder to get, but are not required for passing.
There are just a couple drawbacks in my opinion:

- The exam VM, just like the lab has no Internet access and while it has all needed Windows tools, it often lacks the equivalent Linux tools.
    As someone who is more used to for example `certipy` than `Certify.exe`, this makes some things frustrating.
- The exam follows the course very closely.
    Often reading the course materials felt like cheating, since they are almost a walkthrough (just out of order and with slightly different hostnames).
- I think I saw how to you were supposed to get flag 7, but I did not manage to exploit it.
    But it was possible to get flag 8 first and using that get flag 7 trivially.
    Not sure if that is an intended way 🤷.

### Exam progress

It took me a while in the beginning to set everything up, so I only started to get flags at the eventing of the first day.
From there on it was pretty smooth sailing to get the 6 required flags.
The last two flags took me a lot of time and failed attempts, but I got them by the end of the second day.

![Exam page](../assets/img/crto-exam-graph.png)
