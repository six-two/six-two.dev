---
hide:
- toc
- navigation
---

# Welcome

I am *six-two* a pentester by day and a programmer by night.

- Take a look at some of my [projects](projects.md).
- See some of the [vulnerabilities](disclosures/index.md) I stumbled across and disclosed.
- Contact me via email <a href="mailto:info@six-two.dev">info@six-two.dev</a> or my [contact form](https://contact.six-two.dev).
- Download my [commit signing GPG key](files/commit_signing_key.gpg)
- Hit the back button and leave :)

<!-- Note: malito is a HTML link, because ezlinks is not maintained and has a bug: https://github.com/orbikm/mkdocs-ezlinks-plugin/issues/48 -->
