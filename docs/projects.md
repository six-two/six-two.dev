---
hide:
- toc
- navigation
---

# Projects

I have the following accounts where my projects are published:

|GitHub|six-two|link:https://github.com/six-two/|
|GitLab|six-two|link:https://gitlab.com/six-two/|
|PyPI|six-two|link:https://pypi.org/user/six-two/|

## Web applications

- My older web applications (ReactJS based) are listed at [projects.six-two.dev](https://projects.six-two.dev).

## MkDocs plugins

[MkDocs](https://www.mkdocs.org/) is a static site generator (which is also used to create this web site).
I have written the following plugins:

- [autotag](https://github.com/six-two/mkdocs-auto-tag-plugin): Add tags based on file names and paths
- [badges](https://github.com/six-two/mkdocs-badges): Add badges to your MkDocs page
- [crosslink](https://github.com/six-two/mkdocs-crosslink-plugin): Simplify links between different MkDocs sites
- [placeholder](https://github.com/six-two/mkdocs-placeholder-plugin): This plugin allows you to have placeholders in your site, that can be dynamically replaced at runtime using JavaScript
- [toggle-sidebar](https://github.com/six-two/mkdocs-toggle-sidebar-plugin): Toggle the navigation and/or TOC sidebars on your MkDocs site

## Pentesting

These projects are either written for pentesting or in other ways useful during my normal work:

- [demo-pentest-report](https://gitlab.com/six-two/demo-pentest-report): A very simple report generator using Markdown and pandoc.
- [personal scripts / shell configs](https://gitlab.com/six-two/bin): Some small pentest related script are in `pentest/`.
- [qr.html](https://github.com/six-two/qr.html): Single HTML file QR code generator. Useful to extract small amounts of information (less than 3kB, like the result of a terminal command) from VMs, Citrix, RDP, etc when other methods (shared file system, clipboard sync) are disabled.
- [self-hosted-static-pages](https://github.com/six-two/self-hosted-static-pages): Files / simple web applications to host on a server. Can be self-hosted if you are not allowed to use external resources.
- [self-unzip.html](https://github.com/six-two/self-unzip.html): Self-extracting HTML pages. Useful to smuggle executable files past corporate HTTP proxies that scan downloads or filter them by file type.
- [shell-command-logger](https://github.com/six-two/shell-command-logger): Record the output of Linux commands (using `script`), so that they can be replayed or searched later.
