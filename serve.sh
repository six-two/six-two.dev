#!/usr/bin/env bash

# cd into the repo's root dir
cd "$( dirname "${BASH_SOURCE[0]}" )"

# If user says to update dependencies or we do not see the venv, we update/create it
if [[ "$1" == update || ! -f venv/bin/activate  ]]; then
    if [[ ! -f venv/bin/activate ]]; then
        echo "[*] Creating virtual python environment"
        python3 -m venv --clear --upgrade-deps venv
    fi
    echo "[*] Using virtual python environment"
    source venv/bin/activate

    echo "[*] Updating pip dependencies"
    python3 -m pip install -r requirements.txt
else
    echo "[*] Using virtual python environment"
    source venv/bin/activate
fi

mkdocs serve
