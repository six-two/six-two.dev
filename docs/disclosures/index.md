# Vulnerability disclosures

While I do not actively search for vulnerabilities in my spare time, sometimes I find them by accident.
If it is in (open source) software that I like, I will usually disclose it to the vendor.
Not listed here are advisories published in my day job.

## 2022

- A stored XSS in the MkDocs static site generator.
    This is a low risk vulnerability, since only authors of the pages can exploit it.
    This problem was fixed in [MkDocs version 1.3.0](https://github.com/mkdocs/mkdocs/releases/tag/1.3.0).
    Disclosure via GitHub [issue #2790](https://github.com/mkdocs/mkdocs/issues/2790).[^1]
- Information disclosure and missing authentication in PowerHub.
    Due to PowerHub not authenticating clients, an attacker can pretend to be a client and steal clipboard entries, custom modules (which may contain valuable exploits) and up- and download files.
    These problems were fixed in PowerHub version 2.0.0.
    Disclosure via a small [advisory 2022-001](2022-001.md).

[^1]: If the link will fail in the future, [here](mkdocs-xss-2790.png) is a screenshot.
